package core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

public interface GitObject 
{
	
	public static byte[] getByteObject(String path, String h)
	{
		byte[] result = new byte[500];
		
		String folder = h.substring(0, 2);

		String file = h.substring(2);

		try
		{
			InflaterInputStream iis = new InflaterInputStream(new FileInputStream(path + "/objects/" + folder + "/" + file));
			iis.read(result);
			iis.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return result;
	}
	
	public String getHash();
	
	public String getType();
}