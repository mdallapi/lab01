package core;

import java.util.Arrays;
import javax.xml.bind.DatatypeConverter;

public class GitTreeObject implements GitObject 
{

	private String hash;

	private String gitStringObject;
	
	private byte[] gitByteObject;
	
	private String path;
	
	public GitTreeObject(String repoPath, String h)
	{
		hash = h;
		
		path = repoPath;
		
		gitByteObject = GitObject.getByteObject(path, hash);
		
		gitStringObject = new String(gitByteObject);
	}
	
	public String getHash() 
	{
		return hash;
	}
	
	public String getType()
	{
		return gitStringObject.split(" ")[0];	
	}

	public String[] getEntryPaths() 
	{
		String trimmed = gitStringObject + " ";
		trimmed = trimmed.replaceAll("\\P{Print}.*? ", " ");
		trimmed = trimmed.substring(9);
		String[] result = trimmed.split(" ");
		return result;
	}

	public GitObject getEntry(String s) 
	{
		int start; 
		GitObject result = null;
		
		start = gitStringObject.indexOf(s) + s.length() + 1;
		byte[] bytes = Arrays.copyOfRange(gitByteObject, start, start + 20);
		String file = DatatypeConverter.printHexBinary(bytes).toLowerCase();
		String check = new String(GitObject.getByteObject(path, file)).split(" ")[0];
		if(check.equals("blob"))
		{
			result = new GitBlobObject(path,file);
		}
		if(check.equals("tree"))
		{
			result = new GitTreeObject(path,file);
		}
		if(check.equals("commit"))
		{
			result = new GitCommitObject(path,file);
		}
		return result;
	}
}