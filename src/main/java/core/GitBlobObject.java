package core;

public class GitBlobObject implements GitObject 
{

	private String hash;

	private String gitStringObject;
	
	private String path;
	
	public GitBlobObject(String repoPath, String h)
	{
		hash = h;
		
		path = repoPath;
		
		gitStringObject = new String(GitObject.getByteObject(path, hash));
	}
	
	public String getHash() 
	{
		return hash;
	}
	
	public String getType()
	{
		return gitStringObject.split(" ")[0];	
	}
	
	public String getContent()
	{
		return gitStringObject.substring(8,70);	
	}

}