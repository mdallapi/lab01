package core;

public class GitCommitObject implements GitObject
{
	private String hash;

	private String gitStringObject;
	
	private String path;
	
	public GitCommitObject(String repoPath, String h)
	{
		hash = h;
		
		path = repoPath;
		
		gitStringObject = new String(GitObject.getByteObject(path, hash));
	}
	
	public String getHash() 
	{
		return hash;
	}
	
	public String getType()
	{
		return gitStringObject.split(" ")[0];	
	}

	public String getTreeHash() {
		return gitStringObject.substring(16, 56);
	}

	public String getParentHash() {
		return gitStringObject.substring(64, 104);
	}

	public String getAuthor() {
		return gitStringObject.substring(112, 156);
	}
}