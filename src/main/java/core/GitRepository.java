package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {

	private String repoPath;
	
	public GitRepository()
	{
		repoPath = "";
	}
	
	public GitRepository(String p)
	{
		repoPath = p;
	}
	
	public String getHeadRef()
	{
		String result = "";
		try
		{
		BufferedReader f = new BufferedReader(new FileReader(repoPath+"/HEAD"));
		result = f.readLine().substring(5);
		f.close();
		}
		catch (IOException e)
		{
			result = "file not found";
		}
		return result;
	}
	
	public String getRefHash(String filepath)
	{
		String result;
		try
		{
		BufferedReader f = new BufferedReader(new FileReader(repoPath + "/" + filepath));
		result = f.readLine();
		f.close();
		}
		catch (IOException e)
		{
			result = "file not found";
		}
		return result;
	}
}
